Gem::Specification.new do |s|
  s.name = %q{screenplay_pattern}
  s.version = "0.1.0"
  s.date = %q{2020-08-10}
  s.summary = %q{Ruby implementation of the screenplay pattern.}
  s.authors = ["Andy Palmer", "Rebecca Williams"]
  s.email = %q{screenplay@riverglide.com}
  s.files = ["lib/screenplay_pattern.rb"]
  s.require_paths = ["lib"]
end
