class Actor
  attr_reader :name, :capability

  def initialize(name)
    @name = name
  end

  def self.named name
    new(name)
  end

  def can(capability)
    @capability = capability
  end
end

class Given
  def initialize actor
    @actor = actor
  end

  def self.the_actor actor
    new(actor)
  end

  def was_able_to do_something
    do_something.perform_as(@actor)
  end
end

class When
  def initialize actor
    @actor = actor
  end

  def self.the_actor actor
    new(actor)
  end

  def attempts_to do_something
    do_something.perform_as(@actor)
  end
end

def use_ruby(capability)
  capability
end
